# -*- coding: utf-8 -*-
"""
Created on Tue Apr 23 13:25:40 2019

@author: enovi
"""

import nltk
import datetime
import blingfire
import keras
from spacy.lang.en import English

def main():
    texts = input('Enter the text to tokenize:' )
    nltk_speed = nltk_test(texts)
    blingfire_speed = blingfire_test(texts)
    keras_speed = keras_test(texts)
    spacy_speed = spacy_test(texts)
    
    show_results(nltk_speed,blingfire_speed,keras_speed,spacy_speed)
    
def nltk_test(texts):
    nltk_speed = [0,0]
    #show starting time
    nltk_speed[0] = datetime.datetime.utcnow().timestamp()
    nltk.tokenize.word_tokenize(texts)
    #show ending time
    nltk_speed[1] = datetime.datetime.utcnow().timestamp()
    return nltk_speed[1] - nltk_speed[0]

def blingfire_test(texts):
    blingfire_speed = [0,0]
    blingfire_speed[0] = datetime.datetime.utcnow().timestamp()
    blingfire.text_to_words(texts).split(' ')
    blingfire_speed[1] = datetime.datetime.utcnow().timestamp()
    return blingfire_speed[1] - blingfire_speed[0]

def keras_test(texts):
    keras_speed = [0,0]
    keras_speed[0] = datetime.datetime.utcnow().timestamp()
    keras.preprocessing.text.text_to_word_sequence(texts)
    keras_speed[1] = datetime.datetime.utcnow().timestamp()
    return keras_speed[1] - keras_speed[0]

def spacy_test(texts):
    spacy_speed = [0,0]
    spacy_speed[0] = datetime.datetime.utcnow().timestamp()
    spacy_tokenizer = English().Defaults.create_tokenizer()
    spacy_document = spacy_tokenizer(texts)
    token_list = []
    for token in spacy_document:
        token_list.append(token.text)
    spacy_speed[1] = datetime.datetime.utcnow().timestamp()
    return spacy_speed[1] - spacy_speed[0]
    
def show_results(nltk_speed,blingfire_speed,keras_speed,spacy_speed):
    print(nltk_speed, 'NLTK')
    print(blingfire_speed, 'Blingfire')
    print(keras_speed, 'Keras')
    print(spacy_speed, 'Spacy')
    
main()